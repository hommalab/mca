# Proxy configuration

2022/05/24

--
It appears that the server now requires a proxy to connect top the internet.

## For `apt`
Following `https://www.serverlab.ca/tutorials/linux/administration-linux/how-to-set-the-proxy-for-apt-for-ubuntu-18-04/`

Create the `proxy.conf` file

```bash
sudo touch /etc/apt/apt.conf.d/proxy.conf
```

Edit this file to include the Waseda proxy

```bash
sudo nano /etc/apt/apt.conf.d/proxy.conf
```

Add the proxy configuration and save

```python
Acquire {
  HTTP::proxy "http://www-proxy.waseda.jp:8080";
  HTTPS::proxy "http://www-proxy.waseda.jp:8080";
}
```

##Bash

We are using the bash shell

```bash
echo "$SHELL"
```
which prints
>/bin/bash

To use `curl`, etc. we need to set the Waseda proxy.

To set the proxy for the shell, use 

```bash
export http_proxy='http://www-proxy.waseda.jp:8080/' 
export https_proxy='http://www-proxy.waseda.jp:8080/'
```
We can confirm that we can connect to the outside internet

```bash
ping 8.8.8.8
```

Unfortunately, this has to be done every time the terminal is started, so this is not very convenient.

A better solution is to update the [environment](https://askubuntu.com/questions/175172/how-do-i-configure-proxies-without-gui).

```bash
nano /etc/environment
```
Add
 
```bash
export http_proxy="http://www-proxy.waseda.jp:8080/"
export https_proxy="http://www-proxy.waseda.jp:8080/"
export HTTP_PROXY="http://www-proxy.waseda.jp:8080/"
export HTTPS_PROXY="http://www-proxy.waseda.jp:8080/"
```
save and reboot.

This should apply to all users and only is required once.


