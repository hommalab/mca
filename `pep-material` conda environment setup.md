#  `pep-material` conda environment setup

_2022/05/24_

--

Virtual environments in Python are essential for two reasons - to have a well-define environment that can be documented. - to avoid version conflicts between packages and different versions of Python - to create a reproducible environment on different machines

We can use Anaconda, which installed by default on this machine, to create these environments.

To get started, log into the server's Jupyter lab environment and start a terminal session.

Use
 
```bash
conda  create -n $environment_name tensorflow-gpu
```
to create a new `Conda` environment using tensorflow with GPU acceleration. Replace `$environment_name` with the name of your environment.

At the present moment, this will install `python 3.9` and `tensorflow 2.4`.
You can, in principle specify the version to be installed, e.g. using `python=3.7` as an option for the command above.

For details, see the `conda` [documentation](https://docs.conda.io/projects/conda/en/latest/user-guide/tasks/manage-environments.html#sharing-an-environment).

To verify the installation, activate the environment `conda activate $environment_name` and open the python console `python`.


```python
import tensorflow as tf
tf.config.list_physical_devices(
    device_type=None
)
```
This should list the CPU and 4 GPU devices.

Exit the `python` console using `quit()`.

Next, we need to make this environment available to the Jupyter Server.
With the envirionment activated, execute

```
conda install jupyter
conda install nb_conda
conda install ipykernel
```
You can install addtional packages as needed, either via `conda` or `pip`.

Finally, register the new kernel

```python -m ipykernel install --user --name $myKernelName```,
again replacing `$myKernelName` with a useful name.

The new environment (which is called a kernel in Jupyter) should now show upo in JupyterHub
Quit the environment using `conda deactivate`.

## Uninstall

To remove the environment, first uninstall the kernel. __Activate the environment__ and execute

`jupyter kernelspec uninstall $myKernelName`

You can check which kernels are installed using

`jupyter kernelspec list`

To remove the environment, execute

`conda env remove -n $myEnvironment`

