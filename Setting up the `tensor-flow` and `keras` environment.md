# Setting up the `tensor-flow` and `keras` environment
>2019/06/11

By default, `tensorflow 1.13.1` is installed on `pip-material`, as reported via `conda list`. In principle, this will allow machine learning using `keras`. It might be a good idea, however, to create our own isolated environment. 

To check the `cuda` driver version, enter 
```
nvcc --version
```

This prints 
>nvcc: NVIDIA (R) Cuda compiler driver
Copyright (c) 2005-2017 NVIDIA Corporation
Built on Fri Nov 3 21:07:56 CDT 2017
Cuda compilation tools, release 9.1, V9.1.85

`9.1.85` is actually a bit old, the most recent version is `10.1`.

The installed GPUs can be listed using `nvidia-smi`.

In principle, one should be able to create a new `tensorflow-gpu` environment using

```
conda  create -n $environmentName tensorflow-gpu
```

according to [https://www.anaconda.com/tensorflow-in-anaconda/](). In principle, this should set things up appropriately.

At the moment, however, this fails with a number of errors such as 
>CondaFileIOError: '/home/morten/.conda/pkgs/libgfortran-ng-7.3.0-hdf63c60_0.tar.bz2'. contains unsafe path: share/licenses/libgfortran/RUNTIME.LIBRARY.EXCEPTION

The same thing happens when cloning the base configuration using

```
conda create --name $environmentName --clone base
```
Using `sudo`

```
sudo conda create --name $environmentName --clone base
```
however works. This creates an environment available to all users, unfortunately (in `/opt/anaconda3/envs`).
This is not really ideal, but at least we now have our environment!

We can test the environment by enetring the Python console (`python`) and starting tensorflow

```
import tensorflow as tf
sess = tf.Session(config=tf.ConfigProto(log_device_placement=True))
```

This should print some info about the GPUs.

To register the kernel with Jupyter, first install `nb-conda`

```
conda install nb_conda

```

and register the kernel

```
python -m ipykernel install --user --name $myKernelName

```






