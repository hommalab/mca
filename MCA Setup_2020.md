#MCA

##Optional
If neccessary, install `ensurepip`
```
sudo  apt-get install python3-venv
```


Create a new environmenmt to keep dependencies separate

```bash
python3 -m pip install --user virtualenv
python3 -m venv MCA
```
Here, `MCA` is the name of the new environment.
A good location do do this is your home folder.

##Activate the environment
Activate the environment:

```
source MCA/bin/activate
```

For MCA, the required components are

* numpy
* scikit-learn
* igor
* h5py
* pyMCR
* matplotlib
* ipyfilechooser
* mpld3

All of these can be installed using `pip install $nameOfComponent`

This is not neccessary, however, the MCA package can be installed using 

```bash
pip -v install --upgrade git+https://shinjukunian@bitbucket.org/hommalab/mca.git
```

To install MDT tools, use 
```
pip -v install --upgrade git+https://shinjukunian@bitbucket.org/hommalab/mdt.git
```



in addition, we need to setup the Jupyter Server using

```bash
pip install jupyter
```
and register the kernel

```bash
python -m ipykernel install --user --name MCA
``` 

to make the new environment visible from Jupyterhub.

Additionall, we should install the IPywidgets

```bash
jupyter nbextension install --user --py widgetsnbextension
jupyter nbextension enable widgetsnbextension --user --py
```

You can now start a new Jupyter notebook with the `MCA` kernel and run code like 

```python
from MCA import NanofinderScan

```

The `MCA` repo contains a sample notebook that might be a useful starting point for data exploration. 
To start, let's create a new directory 

```bash
mkdir MCA 
cd MCA
```
We can download the sample notebook using `wget`

```bash
wget https://bitbucket.org/hommalab/mca/raw/HEAD/MCA.ipynb
```

For the MDT functionality, use

```bash
wget https://bitbucket.org/hommalab/mdt/raw/HEAD/MDT.ipynb
```
In the sample notebook, please make sure to set the path to the intensity data (as a 4D Igor binary) and th Raman shift wave (a text file).

~~~python
data="./data/MyExperiment.ibw"
shift="./data/shift.txt"
~~~

Paths are relative to the notebook (which is in `/MCA`).
Next, import the data 

```python
image=PhalanxImage.PhalanxImage(data,shift)
```
Then create a pair of interpolated (equally spaced) spectra and the corresponding wavenumbers with the desired bounds for start and end (use `None` for the full range).

```python
inter,wn = image.interpolatedSpectra(wnStart=None,wnEnd=None)
```
