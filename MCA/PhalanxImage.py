from scipy import interpolate
import numpy as np
from sklearn import (decomposition)

import igor.binarywave as igor
import h5py
import os
from pymcr.mcr import McrAR
import math
from pymcr.regressors import OLS, NNLS
from pymcr.constraints import ConstraintNonneg, ConstraintNorm

class PhalanxImage:

    def __init__(self,data,shift):

        self.shift=np.loadtxt(shift)
        self.fileName=os.path.basename(data)
        self.name=os.path.splitext(self.fileName)[0]
        self.path=os.path.dirname(data)
        igorD=igor.load(data)
        wave=igorD['wave']
        self.data=wave['wData']
        self.dimOffsets=wave["wave_header"]["sfB"]
        self.dimDeltas = wave["wave_header"]["sfA"]

    def scale(self,idx):
        end=self.dimOffsets[idx]+self.dimDeltas[idx]*self.data.shape[idx]
        scale=np.linspace(start=self.dimOffsets[idx],stop=end,num=self.data.shape[idx])
        return scale


    def interpolatedSpectra(self,wnStart=None,wnEnd=None,numPnts=None):
        if wnStart == None:
            wnStart = self.shift[0]
        if wnEnd == None:
            wnEnd = self.shift[-1]
        if numPnts == None:
            numPnts=abs(round(wnEnd-wnStart)+1)

        newX = np.linspace(start=wnStart, stop=wnEnd, num=numPnts)

        interpolator = interpolate.interp1d(self.shift, self.data, axis=3)
        newY = interpolator(newX)
        return newY,newX



def NMF(dataPath,shiftwavePath,startWN=None,endWN=200, l1l2=0,alpha=1,components=5, smoothComponents=0):

    image = PhalanxImage(dataPath, shiftwavePath)

    inter, wn = image.interpolatedSpectra(wnStart=startWN, wnEnd=endWN)
    shape = inter.shape
    flatInter = inter.reshape((shape[0] * shape[1] * shape[2], shape[3]))
    minV = np.amin(flatInter)

    if minV < 0:
        flatInter += abs(minV)

    if smoothComponents>0:
        PCASmooth = decomposition.PCA(n_components=smoothComponents)
        PCASmoothTrans = PCASmooth.fit_transform(flatInter)
        flatInter = PCASmooth.inverse_transform(PCASmoothTrans)

    nmf = decomposition.NMF(init='nndsvd', verbose=True, n_components=components, alpha=alpha, l1_ratio=l1l2, max_iter=1000)
    nmfComponents = nmf.fit_transform(flatInter)
    imageComponents = nmfComponents.reshape((shape[0], shape[1], shape[2], -1))
    spectra = np.vsplit(nmf.components_, nmf.components_.shape[0])
    saveData(image,wn,spectra,imageComponents,suffix="rNMF")

def PCA(dataPath,shiftwavePath,startWN=None,endWN=200,components=5):

    image = PhalanxImage(dataPath, shiftwavePath)

    inter, wn = image.interpolatedSpectra(wnStart=startWN, wnEnd=endWN)
    shape = inter.shape
    flatInter = inter.reshape((shape[0] * shape[1] * shape[2], shape[3]))
    

    pca = decomposition.PCA(n_components=components)
    nmfComponents = pca.fit_transform(flatInter)
    imageComponents = nmfComponents.reshape((shape[0], shape[1], shape[2], -1))
    spectra = np.vsplit(pca.components_, pca.components_.shape[0])
    saveData(image,wn,spectra,imageComponents,suffix="rPCA")


def MCAALS(dataPath,shiftwavePath,startWN=None,endWN=200,components=5,smoothComponents=0):

    image = PhalanxImage(dataPath, shiftwavePath)

    inter, wn = image.interpolatedSpectra(wnStart=startWN,wnEnd=endWN)
    shape = inter.shape
    flatInter = inter.reshape((shape[0] * shape[1] * shape[2], shape[3]))
    minV = np.amin(flatInter)

    if minV < 0:
        flatInter += abs(minV)

    if smoothComponents>0:
        PCASmooth = decomposition.PCA(n_components=smoothComponents)
        PCASmoothTrans = PCASmooth.fit_transform(flatInter)
        flatInter = PCASmooth.inverse_transform(PCASmoothTrans)

    n_components=components
    svd = decomposition.TruncatedSVD(n_components=n_components)
    svd.fit_transform(flatInter)


    # initialSpectralGuesses=np.random.rand(n_components,flatInter.shape[1])
    #
    # initialSpectralGuesses *= math.sqrt(np.mean(flatInter)/n_components)
    initialSpectralGuesses=abs(svd.components_)
    mcrALS=McrAls(max_iter=100,tol_increase=1000)
    mcrALS.fit(flatInter,ST=initialSpectralGuesses,verbose=True)
    print('\nFinal MSE: {:.7e}'.format(mcrALS.err[-1]))
    spectra=mcrALS.ST_opt_
    imageComponents=mcrALS.C_opt_.reshape((shape[0], shape[1], shape[2], -1))

    saveData(image,wn,spectra,imageComponents,suffix="_rMCAALS")


def saveData(phalanxImage, shiftWave, spectra, imageComponents, spectraFileName = None, suffix = "_result"):

    if spectraFileName is None:
        spectraFileName = phalanxImage.path + "/" + phalanxImage.name + suffix + ".hdf5"

    f = h5py.File(spectraFileName, "w")

    grp_spectra = f.create_group("spectra")
    grp_spectra.create_dataset("shift", data=shiftWave)


    for idx, spectrum in enumerate(spectra):
        name = "spectrum_{}".format(idx)
        grp_spectra.create_dataset(name, data=spectrum.flatten())

    grp_images = f.create_group("images")
    grp_scales=grp_images.create_group("dimScales")

    images = np.split(imageComponents, imageComponents.shape[3], axis=3)

    firstImage=images[0]
    dimScales=[]
    scaleData=[[0,0]]

    for idx, shape in enumerate(firstImage.shape):
        if shape>1:
            scale=phalanxImage.scale(idx)
            scaleEntry=grp_scales.create_dataset("dim_{}".format(idx),data=scale)
            #https://github.com/h5py/h5py/issues/643, scales have to be attached to a dataset
            dimScales.append(scaleEntry)
            scaleData.append([phalanxImage.dimDeltas[idx], phalanxImage.dimOffsets[0]])

    for idx, image in enumerate(images):

        sq = np.squeeze(image)
        dSet=grp_images.create_dataset("image_{}".format(idx), data=sq)

        dSet.attrs["IGORWaveScaling"]=scaleData

    f.flush()
    f.close()


def reconstructFromWaves(dataPath, shiftwavePath, initialGuessesPath,startWN=None,endWN=None):
    image = PhalanxImage(dataPath, shiftwavePath)

    inter, wn = image.interpolatedSpectra(wnStart=startWN, wnEnd=endWN)
    shape = inter.shape
    flatInter = inter.reshape((shape[0] * shape[1] * shape[2], shape[3]))
    minV = np.amin(flatInter)

    if minV < 0:
        flatInter += abs(minV)

    initialSpectralGuesses = np.loadtxt(initialGuessesPath)


    if startWN == None:
        startWN = image.shift[0]
    if endWN == None:
        endWN = image.shift[-1]

    newX = np.linspace(start=startWN, stop=endWN, num=abs(round(endWN-startWN)+1))

    interpolator = interpolate.interp1d(image.shift, initialSpectralGuesses, axis=0)
    initialSpectralGuesses = interpolator(newX)

    mcrALS = McrAls(max_iter=100, tol_increase=1000)
    mcrALS.fit(flatInter, ST=np.transpose(initialSpectralGuesses), verbose=True)
    print('\nFinal MSE: {:.7e}'.format(mcrALS.err[-1]))
    spectra = mcrALS.ST_opt_
    imageComponents = mcrALS.C_opt_.reshape((shape[0], shape[1], shape[2], -1))

    saveData(image, wn, spectra, imageComponents, suffix="_rMCAALS")






# reconstructFromWaves(dataPath="/Volumes/Samsung_T3/suzuki/20181226/hela5.ibw",
#                      shiftwavePath="/Volumes/Samsung_T3/suzuki/20181226/shiftwave.txt",
#                      initialGuessesPath="/Users/morten/Documents/Raman/201807_analysisLecture/initialGuesses.txt",
#                      startWN=400)

# MCAALS(dataPath="/Users/morten/Documents/Raman/Suzuki/20181226/hela5.ibw",
#        shiftwavePath="/Users/morten/Documents/Raman/Suzuki/20181226/shiftwave.txt",
#        startWN=None,endWN=350,components=4)

# analyzeSpectra(dataPath="/Users/morten/Documents/Raman/Suzuki/20181226/hela5.ibw",
#                shiftwavePath="/Users/morten/Documents/Raman/Suzuki/20181226/shiftwave.txt",
#                startWN=None,endWN=350,alpha=0.1,l1l2=1,components=4)




