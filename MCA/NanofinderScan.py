import igor.binarywave as igor
from scipy import interpolate
import numpy as np
from sklearn import (decomposition)
import h5py
import os
from pymcr.mcr import McrAR

class NanofinderScan:

    def __init__(self,data,shift):

        self.fileName=os.path.basename(data)
        self.name=os.path.splitext(self.fileName)[0]
        self.path=os.path.dirname(data)
        igorD=igor.load(data)
        wave=igorD['wave']
        self.data=wave['wData']
        self.dimOffsets=wave["wave_header"]["sfB"]
        self.dimDeltas = wave["wave_header"]["sfA"]
        self.dataShape=self.data.shape
        shiftD=igor.load(shift)
        self.shift=shiftD['wave']['wData']
        self.numberOfSpectra=self.dataShape[0]*self.dataShape[1]*self.dataShape[2]

    def scale(self,idx):
        end=self.dimOffsets[idx]+self.dimDeltas[idx]*self.data.shape[idx]
        scale=np.linspace(start=self.dimOffsets[idx],stop=end,num=self.data.shape[idx])
        return scale


    def interpolatedSpectra(self,wnStart=None,wnEnd=None,numPnts=None):
        if wnStart == None:
            wnStart = self.shift[0]
        if wnEnd == None:
            wnEnd = self.shift[-1]
        if numPnts == None:
            numPnts=abs(round(wnEnd-wnStart)+1)

        newX = np.linspace(start=wnStart, stop=wnEnd, num=numPnts)

        interpolator = interpolate.interp1d(self.shift, self.data, axis=3)
        newY = interpolator(newX)
        return newY,newX

    def saveResult(self, shiftWave, spectra, imageComponents, suffix="_result"):

        spectraFileName = self.path + "/" + self.name + suffix + ".hdf5"
        f = h5py.File(spectraFileName, "w")

        grp_spectra = f.create_group("spectra")
        grp_spectra.create_dataset("shift", data=shiftWave)

        for idx, spectrum in enumerate(spectra):
            name = "spectrum_{}".format(idx)
            grp_spectra.create_dataset(name, data=spectrum.flatten())

        grp_images = f.create_group("images")
        grp_scales = grp_images.create_group("dimScales")

        images = np.split(imageComponents, imageComponents.shape[3], axis=3)

        firstImage = images[0]
        dimScales = []

        for idx, shape in enumerate(firstImage.shape):
            if shape > 1:
                scale = self.scale(idx)
                scaleEntry = grp_scales.create_dataset("dim_{}".format(idx), data=scale)
                # https://github.com/h5py/h5py/issues/643, scales have to be attached to a dataset
                dimScales.append(scaleEntry)

        for idx, image in enumerate(images):

            sq = np.squeeze(image)
            dSet = grp_images.create_dataset("image_{}".format(idx), data=sq)

            if idx == 0:
                for scale in dimScales:
                    dSet.dims.create_scale(scale)

            for idx1, dim in enumerate(dSet.dims):
                dim.attach_scale(dimScales[idx1])

        f.flush()
        f.close()



def loadData(path):
    files = os.listdir(path)
    samples = []
    for (file) in files:
        p = path + "/" + file

        if file.startswith("."):
            continue

        extension = os.path.splitext(p)[1]
        filename = os.path.split(p)[1]
        shiftName=""
        if "_d" in filename:
            shiftName=filename.replace("_d","_sh",)
            shiftPath=path + "/" + shiftName
            if os.path.isfile(shiftPath):
                scan=NanofinderScan(data=p,shift=shiftPath)
                samples.append(scan)

    return samples


# def analyzeSpectra(dataPath,shiftwavePath,startWN=None,endWN=200, l1l2=0,alpha=1,components=5, smoothComponents=0):
#
#     # data = "/Users/morten/Desktop/yeast/yeast_PS_z3.ibw"
#     # shift = "/Users/morten/Desktop/yeast/RamanShift.txt"
#
#
#     inter, wn = image.interpolatedSpectra(wnStart=startWN, wnEnd=endWN)
#     shape = inter.shape
#     flatInter = inter.reshape((shape[0] * shape[1] * shape[2], shape[3]))
#     minV = np.amin(flatInter)
#
#     if minV < 0:
#         flatInter += abs(minV)
#
#     if smoothComponents>0:
#         PCASmooth = decomposition.PCA(n_components=smoothComponents)
#         PCASmoothTrans = PCASmooth.fit_transform(flatInter)
#         flatInter = PCASmooth.inverse_transform(PCASmoothTrans)
#
#     nmf = decomposition.NMF(init='nndsvd', verbose=True, n_components=components, alpha=alpha, l1_ratio=l1l2, max_iter=1000)
#     nmfComponents = nmf.fit_transform(flatInter)
#     imageComponents = nmfComponents.reshape((shape[0], shape[1], shape[2], -1))
#     spectra = np.vsplit(nmf.components_, nmf.components_.shape[0])
#     saveData(image,wn,spectra,imageComponents,suffix="rNMF")

def analyzeSpectra_NMF(samples,startWN=None,endWN=200,numPnts=None, l1l2=0,alpha=1,components=5, smoothComponents=0,save=False):
    interpolatedSpectra=[]
    wavenumbers=[]
    originalPoints=[]
    totalPoints=0
    for sample in samples:
        inter, wn = sample.interpolatedSpectra(wnStart=startWN, wnEnd=endWN,numPnts=numPnts)
        shape=inter.shape
        flatInter = inter.reshape((shape[0] * shape[1] * shape[2], shape[3]))
        minV = np.amin(flatInter)
        if minV < 0:
            flatInter += abs(minV)
        wavenumbers.append(wn)
        interpolatedSpectra.append(flatInter)
        originalPoints.append(sample.numberOfSpectra+totalPoints)
        totalPoints+=sample.numberOfSpectra

    spectra=np.vstack(interpolatedSpectra)
    nmf = decomposition.NMF(init='nndsvd', verbose=True, n_components=components, alpha=alpha, l1_ratio=l1l2,
                            max_iter=1000)
    nmfComponents = nmf.fit_transform(spectra)
    splitComponents=np.vsplit(nmfComponents,originalPoints)[:-1]
    spectra = np.vsplit(nmf.components_, nmf.components_.shape[0])
    result=[]
    for sample, components in zip(samples,splitComponents):
        originalShape=sample.dataShape
        reshapedComponents=components.reshape(originalShape[0],originalShape[1],originalShape[2],-1)
        if save is True:
            sample.saveResult(shiftWave=wavenumbers[0], spectra=spectra, imageComponents=reshapedComponents,
                          suffix="_resultNMF")
        result.append(reshapedComponents)

    return samples, result, spectra,wavenumbers



#samples=loadData("/Volumes/Samsung_T3/20190523/data")
#samples,results,spectra,wavenumbers=analyzeSpectra_NMF(samples=samples,startWN=None,endWN=None,numPnts=1600,save=True, components=3)


