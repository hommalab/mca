#MCA

After creating a new environment (`conda create -n $myEnvironment python=3.7`), activate the environment using `conda activate $myEnvironment`.

For MCA, the required components are

* numpy
* scikit-learn
* igor
* h5py
* pyMCR
* matplotlib

All of these can be installed using `pip install $nameOfComponent`

This is not neccessary, however, the MCA package can be installed using 

```bash
pip -v install git+https://shinjukunian@bitbucket.org/hommalab/mca.git
```

in addition, we need to setup the Jupyter Server using

```bash
conda install jupyter
conda install nb_conda
```
and register the kernel

```bash
python -m ipykernel install --user --name $myKernelName
``` 

to make the new environment visible from Jupyterhub.

You can now start a new Jupyter notebook with the `MCA` kernel and run code like 

```python
from MCA import NanofinderScan

```

The `MCA` repo contains a sample notebook that might be a useful starting point for data exploration. 
To start, let's create a new directory 

```bash
mkdir MCA 
cd MCA
```
We can download the sample notebook using `wget`

```bash
wget https://bitbucket.org/hommalab/mca/raw/HEAD/MCA.ipynb
```
In the sample notebook, please make sure to set the path to the intensity data (as a 4D Igor binary) and th Raman shift wave (a text file).

~~~python
data="./data/MyExperiment.ibw"
shift="./data/shift.txt"
~~~

Paths are relative to the notebook (which is in `/MCA`).
Next, import the data 

```python
image=PhalanxImage.PhalanxImage(data,shift)
```
Then create a pair of interpolated (equally spaced) spectra and the corresponding wavenumbers with the desired bounds for start and end (use `None` for the full range).

```python
inter,wn = image.interpolatedSpectra(wnStart=None,wnEnd=None)
```
