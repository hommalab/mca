from setuptools import setup

setup(
    name="MCA",
    version="0.1",
    packages=["MCA"],
    #url='https://github.com/XxdpavelxX/myapp',jupyter nbextension enable --py widgetsnbextension
    install_requires=[
        "numpy",
        "scipy",
        "igor",
        "h5py",
        "scikit-learn",
        "pymcr",
        "matplotlib",
        "ipyfilechooser",
        "mpld3",
    ]
)
