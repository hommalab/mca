#  `pep-material` `tensorflow` environment setup

_2022/06/28_

--

This follows the official [tensorflow guide](https://www.tensorflow.org/install/pip).

Create a new environment using `conda`

```bash
conda create --name $MYENVIRONMENT python=3.9
```
where `$MYENVIRONMENT` is a name you chose.

Activate this environment using `conda activate $MYENVIRONMENT`.
The official guide states to check whether the GPU driver is installed. This can be verified using `nvidia-smi`. 

On `pep-material`, this prints something like
 
```
+-----------------------------------------------------------------------------+
| NVIDIA-SMI 470.129.06   Driver Version: 470.129.06   CUDA Version: 11.4     |
|-------------------------------+----------------------+----------------------+
| GPU  Name        Persistence-M| Bus-Id        Disp.A | Volatile Uncorr. ECC |
| Fan  Temp  Perf  Pwr:Usage/Cap|         Memory-Usage | GPU-Util  Compute M. |
|                               |                      |               MIG M. |
|===============================+======================+======================|
|   0  Tesla V100-PCIE...  Off  | 00000000:3D:00.0 Off |                    0 |
| N/A   40C    P0    26W / 250W |      0MiB / 16160MiB |      0%      Default |
|                               |                      |                  N/A |
+-------------------------------+----------------------+----------------------+
|   1  Tesla V100-PCIE...  Off  | 00000000:3F:00.0 Off |                    0 |
| N/A   40C    P0    25W / 250W |      0MiB / 16160MiB |      0%      Default |
|                               |                      |                  N/A |
+-------------------------------+----------------------+----------------------+
|   2  Tesla V100-PCIE...  Off  | 00000000:40:00.0 Off |                    0 |
| N/A   42C    P0    31W / 250W |      0MiB / 16160MiB |      0%      Default |
|                               |                      |                  N/A |
+-------------------------------+----------------------+----------------------+
|   3  Tesla V100-PCIE...  Off  | 00000000:41:00.0 Off |                    0 |
| N/A   42C    P0    28W / 250W |      0MiB / 16160MiB |      0%      Default |
|                               |                      |                  N/A |
+-------------------------------+----------------------+----------------------+
                                                                               
+-----------------------------------------------------------------------------+
| Processes:                                                                  |
|  GPU   GI   CI        PID   Type   Process name                  GPU Memory |
|        ID   ID                                                   Usage      |
|=============================================================================|
|  No running processes found                                                 |
+-----------------------------------------------------------------------------+

```

Next, install [CUDAToolkit](https://developer.nvidia.com/cuda-toolkit) & [cuDNN](https://developer.nvidia.com/cudnn). 

```bash
conda install -c conda-forge cudatoolkit=11.2 cudnn=8.1.0
```
This will take a while.

CUDA has to be added to the system paths. This can be done using 

```bash
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$CONDA_PREFIX/lib/
```

Obviously, we don't want to do this every time we use the system. The following commands will add the CUDA to the system path every time the environment is activated.

```bash
mkdir -p $CONDA_PREFIX/etc/conda/activate.d
echo 'export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$CONDA_PREFIX/lib/' > $CONDA_PREFIX/etc/conda/activate.d/env_vars.sh
```

This will work when the environment is activated in the terminal. It seems, however, that the `JupyterHub` server uses a different `$LD_LIBRARY_PATH`.

A workaround is to add the `LD_LIBRARY_PATH` to `jupyter_notebook_config.py`. 

First, we need to install the Jupyter environment

```bash
pip install jupyter
```

The `jupyter_notebook_config.py` file can be generated (with default parameters) using `jupyter notebook --generate-config` and is located in `jupyter --config-dir`, typically `~/.jupyter/jupyter_notebook_config.py`.

Add these lines to the top of `jupyter_notebook_config.py`

```python
import os
c = get_config()
os.environ['LD_LIBRARY_PATH'] = _PATHTOCUDA_
c.Spawner.env.update('LD_LIBRARY_PATH')
```

where `_PATHTOCUDA_` is the output of `echo $CONDA_PREFIX/lib/`. This is discussed [here](https://github.com/jupyter/notebook/issues/1290#issuecomment-204388895) and [here](https://wiki.ucar.edu/display/AIML/Add+CUDA+Library+Path+to+Jupyterhub+Notebook).

This workaround will unfortunately use the same CUDA install for all kernels. The last line `c.Spawner.env.update('LD_LIBRARY_PATH')` is likely required since we are using _JupyterHub_. [Adding the `LD_LIBRARY_PATH` to `kernel.json`](https://docs.nersc.gov/services/jupyter/#customizing-kernels-with-a-helper-shell-script), which would allow a custom `LD_LIBRARY_PATH` for different kernels, __didn't work__ (this was also suggested [here](https://github.com/jupyter/notebook/issues/2120#issuecomment-462021570)).

>It is likely possible to add more than one path to `os.environ['LD_LIBRARY_PATH']` to allow different environments with Cuda though it is likely hard to controll which CUDA will be linked.

A restart of the `JupyterHub` server (via Control Panel -> Stop My Server in the web console) is required to have `JupyterHub` pick up the ammended configuration file. You can verify the updated configuration via

```python
import os
import pprint
pprint.pprint(dict(os.environ), width = 1)
```

This should contain the `LD_LIBRARY_PATH `. If it doesn, additional restarts / logout+logins might help.

The `tensorflow `guide states to use `pip` to install tensorflow since the `conda` releases are outdated.

```bash
pip install tensorflow
```
This will install `tensorflow` 2.9.1 (_as of 2022/06/07_).

To verify the setup, use 

```bash 
python3 -c "import tensorflow as tf; print(tf.reduce_sum(tf.random.normal([1000, 1000])))"
```

To verify the GPU installation, use

```bash
python3 -c "import tensorflow as tf; print(tf.config.list_physical_devices('GPU'))"
```

This should print 
>[PhysicalDevice(name='/physical_device:GPU:0', device_type='GPU'), PhysicalDevice(name='/physical_device:GPU:1', device_type='GPU'), PhysicalDevice(name='/physical_device:GPU:2', device_type='GPU'), PhysicalDevice(name='/physical_device:GPU:3', device_type='GPU')]

To install additional dependencies, it is likely better to use `pip` rather than `conda`. The two environment managers don't really mix too well, apparently. Also, avoid using user-local installs. 

For `scikit-learn`, use

```bash
pip install scikit-learn
```

For `matplotlib`, use

```bash
pip install matplotlib
```

At last, you can register the kernel

```bash
python -m ipykernel install --user --name $myKernelName
```
again replacing `$myKernelName` with a useful name.

## Uninstall

To remove the environment, first uninstall the kernel. __Activate the environment__ and execute

`jupyter kernelspec uninstall $myKernelName`

You can check which kernels are installed using

`jupyter kernelspec list`

To remove the environment, execute

`conda env remove -n $myEnvironment`



